/*
Roto-RS - A To-Do App Built in Rust
Copyright (C) 2022  Jackson Novak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// Local modules.
mod input;
mod populate_fs_read;

// Use statements.
use colored::Colorize;
use input::input;
use std::env;
use std::fs;
use std::fs::remove_file;
use std::fs::File;
use std::io::prelude::*;
use std::process::Command;
use populate_fs_read::*;

// Get OS info.
const OS: &str = env::consts::OS;

fn main() {
    // Declare init variables.
    let mut print_history: Vec<String> = Vec::new();

    // Commands.
    let commands = [
        "help",
        "exit",
        "load",
        "done",
        "add",
        "remove",
        "toggle",
        "clear-list",
        "create-list",
        "delete-list",
        "rename-list",
        "list"
    ];

    // Command descriptions.
    let commands_desc = [
        "Show/hide the commands list.",
        "Quit the program.",
        "Specify a list to load.",
        "Show/hide all the tasks that are marked as done.",
        "Add a task.",
        "Remove a task.",
        "Toggle a task.",
        "Clear the entire list. (Be careful!!!)",
        "Create a list.",
        "Delete a list.",
        "Rename a list.",
        "Automatically loads or creates a list for you."
    ];

    // Variables for the main loop.
    let mut error: Vec<String> = Vec::new();
    let mut show_help_list = false;
    let mut list_loaded = false;
    let mut tasks: Vec<String> = Vec::new();
    let mut show_done = true;
    let mut list_name = String::from(":no_list:");

    // The main loop.
    loop {
        clear_terminal();

        let proceed = false;

        // Print history for misc text.
        for i in print_history.iter() {
            println!("{}", i);
        }

        // More print history stuff.
        if print_history.len() != 0 {
            print_line();
            println!("\n\n");
        }

        println!("{}", "<~~ Welcome to Roto-RS: A Rust To-Do App ~~>".bold().truecolor(255, 150, 150));

        if list_loaded == true {
            for i in 0..tasks.len() {
                if tasks[i] == "" {
                    tasks.remove(i);
                }
            }
        }

        if list_loaded == false {
            println!("\n{}\n", "No list is loaded.".red());
        } else {
            println!("");
            print_line_with_text("~[ List Tasks ]~");
            for i in 0..tasks.len() {
                let task_number = i + 1;
                if tasks[i].starts_with("todo > ") {
                    println!(
                        "  {} {arrow} {} {}",
                        task_number.to_string().truecolor(205, 100, 205),
                        "[ ]".cyan(),
                        tasks[i][7..].truecolor(255, 255, 150),
                        arrow = "~>".truecolor(70, 70, 70)
                    );
                } else if tasks[i].starts_with("done > ") {
                    if show_done == true {
                        println!(
                            "  {} {arrow} {} {}",
                            task_number.to_string().truecolor(205, 100, 205),
                            "[x]".cyan(),
                            tasks[i][7..].truecolor(150, 255, 150),
                            arrow = "~>".truecolor(70, 70, 70)
                        );
                    } else {
                        println!("{}", "  ...".truecolor(255, 150, 200));
                    }
                } else {
                    println!(
                        "  {} {arrow} {} {}",
                        task_number.to_string().truecolor(255, 125, 125),
                        "[?]".truecolor(255, 150, 150),
                        tasks[i].truecolor(255, 150, 150),
                        arrow = "!!".truecolor(170, 100, 100)
                    );
                }
            }
            print_line();
            println!("");
        }

        // Show help list.
        if show_help_list == true {
            print_line_with_text("~[ Commands ]~");
            for i in 0..commands.len() {
                println!(
                    "{number} {arrow} {} {arrow} {}",
                    commands[i].truecolor(150, 250, 150),
                    commands_desc[i].truecolor(255, 255, 150),
                    arrow = "~>".truecolor(130, 150, 50),
                    number = (i + 1).to_string().truecolor(250, 150, 150)
                );
            }
            print_line();
            println!("");
        }

        // Print errors.
        if show_help_list == false {
            println!("");
        }
        for i in error.iter() {
            println!("{} {} {}", "[Error]".truecolor(255, 150, 150), "~>".truecolor(70, 70, 70), i.red());
        }

        // Get user input, and turn it into a broken up command.
        println!("");
        error = Vec::new();
        let command_raw = input("Command ~>".yellow().to_string().as_str(), true);
        let mut command: Vec<String> = Vec::new();
        for i in command_raw.split(" ") {
            command.push(i.to_string());
        }

        // Command Execution
        if command[0] == commands[0] {
            if command.len() == 1 {
                show_help_list = !show_help_list;
            } else {
                error.push("That command takes no arguments!".to_string());
            }
        } else if command[0] == commands[1] {
            if command.len() == 1 {
                clear_terminal();
                break;
            } else {
                error.push("That command takes no arguments!".to_string());
            }
        } else if command[0] == commands[2] {
            if command.len() < 2 {
                error.push(
                    "Not enough arguments! That command takes 1 or more arguments!".to_string(),
                );
            } else {
                let mut phrase = String::new();
                for i in 1..command.len() {
                    phrase.push_str(command[i].as_str());
                    if i != command.len() - 1 {
                        phrase.push_str(" ");
                    }
                }
                list_name = string_to_snake_case(phrase.as_str());
                let mut success = false;
                let mut file = match File::open(format!("{}.tdl", list_name)) {
                    Ok(o) => {
                        success = true;
                    }
                    Err(e) => {
                        error.push(format!("The list file '{}.tdl' could not be loaded!", list_name));
                    }
                };
                if success == true {
                    let file_data_raw = fs::read_to_string(format!("{}.tdl", list_name)).expect("failed to read file");
                    tasks = read_to_tasks(file_data_raw);
                    list_loaded = true;
                }
            }
        } else if command[0] == commands[3] {
            if command.len() == 1 {
                show_done = !show_done;
            } else {
                error.push("That command takes no arguments!".to_string());
            }
        } else if command[0] == commands[4] {
            if command.len() < 2 {
                error.push("That command takes 1 or more arguments!".to_string());
            } else {
                if list_loaded == true {
                    let mut phrase = String::from("todo > ");
                    for i in 1..command.len() {
                        phrase.push_str(command[i].as_str());
                        if i != command.len() - 1 {
                            phrase.push_str(" ");
                        }
                    }
                    tasks.push(phrase);

                    save_list(list_name.as_str(), &tasks);
                } else {
                    error.push("You don't have a list loaded!".to_string());
                }
            }
        } else if command[0] == commands[5] {
            if command.len() != 2 {
                error.push("That command takes 1 argument!".to_string());
            } else {
                if list_loaded == false {
                    error.push("You don't have a list loaded!".to_string());
                } else {
                    let my_num = command[1].parse::<usize>().unwrap();
                    tasks.remove(my_num - 1);

                    save_list(list_name.as_str(), &tasks);
                }
            }
        } else if command[0] == commands[6] {
            if command.len() != 2 {
                error.push("That command takes 1 argument!".to_string());
            } else {
                if list_loaded == false {
                    error.push("You don't have a list loaded!".to_string());
                } else {
                    let my_num = command[1].parse::<usize>().unwrap();
                    let target_item = my_num - 1;

                    if tasks[target_item].starts_with("todo > ") {
                        let my_text = &tasks[target_item][7..];
                        tasks[target_item] = format!("done > {}", my_text);
                    } else if tasks[target_item].starts_with("done > ") {
                        let my_text = &tasks[target_item][7..];
                        tasks[target_item] = format!("todo > {}", my_text);
                    } else {
                        error.push(format!(
                            "Task number {} is corrupted! Check the '{}.tdl' file!",
                            my_num, list_name
                        ));
                    }

                    save_list(list_name.as_str(), &tasks);
                }
            }
        } else if command[0] == commands[7] {
            if command.len() != 1 {
                error.push("That command takes no arguments!".to_string());
            } else {
                if list_loaded == false {
                    error.push("You don't have a list loaded!".to_string());
                } else {
                    let answer = input("Are you sure you want to continue? There is no going back if you continue! [y/N]:".yellow().to_string().as_str(), true).to_lowercase();
                    let mut proceed = false;

                    if answer == "n" || answer == "" {
                        proceed = false;
                    } else if answer == "y" {
                        let answer = input(
                            "Are you sure? [y/N]:"
                                .truecolor(255, 150, 150)
                                .to_string()
                                .as_str(),
                            true,
                        )
                        .to_lowercase();
                        if answer == "n" || answer == "" {
                            proceed = false;
                        } else if answer == "y" {
                            proceed = true;
                        } else {
                            error.push(
                                "That is not a valid response! Defaulting to \"no\"!".to_string(),
                            );
                            proceed = false;
                        }
                    } else {
                        error.push(
                            "That is not a valid response! Defaulting to \"no\"!".to_string(),
                        );
                        proceed = false;
                    }

                    if proceed == true {
                        tasks = Vec::new();
                    }
                }
            }
        } else if command[0] == commands[8] {
            if command.len() < 2 {
                error.push("That command takes 1 or more arguments!".to_string());
            } else {
                let mut phrase = String::new();
                for i in 1..command.len() {
                    phrase.push_str(command[i].as_str());
                    if i != command.len() - 1 {
                        phrase.push_str("_");
                    }
                }
                let mut file =
                    File::create(format!("{}.tdl", phrase)).expect("failed to create file");
            }
        } else if command[0] == commands[9] {
            if command.len() < 2 {
                error.push("That command takes 1 or more arguments!".to_string());
            } else {
                let answer = input("Are you sure you want to continue? There is no going back if you continue! [y/N]:".yellow().to_string().as_str(), true).to_lowercase();
                let mut proceed = false;

                if answer == "n" || answer == "" {
                    proceed = false;
                } else if answer == "y" {
                    let answer = input(
                        "Are you sure? [y/N]:"
                            .truecolor(255, 150, 150)
                            .to_string()
                            .as_str(),
                        true,
                    )
                    .to_lowercase();
                    if answer == "n" || answer == "" {
                        proceed = false;
                    } else if answer == "y" {
                        proceed = true;
                    } else {
                        error.push(
                            "That is not a valid response! Defaulting to \"no\"!".to_string(),
                        );
                        proceed = false;
                    }
                } else {
                    error.push("That is not a valid response! Defaulting to \"no\"!".to_string());
                    proceed = false;
                }

                if proceed == true {
                    let mut phrase = String::new();
                    for i in 1..command.len() {
                        phrase.push_str(command[i].as_str());
                        if i != command.len() - 1 {
                            phrase.push_str("_");
                        }
                    }
                    remove_file(format!("{}.tdl", phrase)).expect("failed to delete file");
                }
            }
        } else if command[0] == commands[10] {
            if command.len() != 3 {
                error.push("That command takes 2 arguments! Keep in mind that file names are stored in \"snake_case\"!".to_string());
            } else {
                let str1 = format!("{}.tdl", string_to_snake_case(command[1].as_str()));
                let str2 = format!("{}.tdl", string_to_snake_case(command[2].as_str()));
                fs::rename(&str1, &str2).expect("failed to rename file");
                clear_terminal();
                println!("{}", "Restart app.".yellow());
                break;
            }
        } else if command[0] == commands[11] {
            if command.len() < 2 {
                error.push(
                    "Not enough arguments! That command takes 1 or more arguments!".to_string(),
                );
            } else {
                let mut phrase = String::new();
                for i in 1..command.len() {
                    phrase.push_str(command[i].as_str());
                    if i != command.len() - 1 {
                        phrase.push_str(" ");
                    }
                }
                list_name = string_to_snake_case(phrase.as_str());
                let file_data_raw = populate_read_to_string(format!("{}.tdl", list_name).as_str(), "");
                tasks = read_to_tasks(file_data_raw);
                list_loaded = true;
            }
        }

        // Error Handling
        else if command[0] == "" && command.len() == 1 {
            // Pass
        } else {
            error.push(format!(
                "That is not a valid command! Use {} to see all available commands!",
                &commands[0]
            ));
        }
    }
}

fn print_line() {
    print_line_with_text("");
}

fn print_line_with_text(text: &str) {
    let size: (usize, usize) = term_size::dimensions().unwrap();
    let text_length = text.len();
    let mut phrase = String::from(text).bold().italic().truecolor(50, 200, 200).to_string();
    for i in 0..(size.0 - text_length) {
        phrase.push_str("-".truecolor(50, 200, 200).to_string().as_str());
    }
    println!("{}", phrase);
}

fn clear_terminal() {
    if OS == "windows" {
        Command::new("cls").status().unwrap();
    } else {
        Command::new("clear").status().unwrap();
    }
}

fn read_to_tasks(data: String) -> Vec<String> {
    let mut return_vec: Vec<String> = Vec::new();

    for i in data.split("\n") {
        return_vec.push(i.to_string());
    }

    return return_vec;
}

fn string_to_snake_case(text: &str) -> String {
    let mut phrase = String::new();

    for i in text.chars() {
        if i == ' ' {
            phrase.push_str("_");
        } else {
            phrase.push_str(i.to_string().as_str());
        }
    }

    return phrase;
}

fn save_list(list_name: &str, tasks: &Vec<String>) {
    let mut phrase = String::new();
    for i in 0..tasks.len() {
        phrase.push_str(tasks[i].as_str());
        if i != tasks.len() - 1 {
            phrase.push_str("\n");
        }
    }
    let mut file = File::create(format!("{}.tdl", list_name)).expect("failed to create file");
    file.write_all(phrase.as_str().as_bytes())
        .expect("failed to write to file");
}
